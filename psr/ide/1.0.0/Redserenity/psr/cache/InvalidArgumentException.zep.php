<?php

namespace RedSerenity\Psr\Cache;

/**
 * Exception interface for invalid cache arguments.
 *
 * Any time an invalid argument is passed into a method it must throw an
 * exception class which implements Psr\Cache\InvalidArgumentException.
 *
 * Interface InvalidArgumentException
 */
interface InvalidArgumentException extends \RedSerenity\Psr\Cache\CacheException
{

}
