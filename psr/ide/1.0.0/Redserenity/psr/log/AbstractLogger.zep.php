<?php

namespace RedSerenity\Psr\Log;

/**
 * This is a simple Logger implementation that other Loggers can inherit from.
 *
 * It simply delegates all log-level-specific methods to the `log` method to
 * reduce boilerplate code that a simple Logger that does the same thing with
 * messages regardless of the error level has to implement.
 *
 * Class AbstractLogger
 */
abstract class AbstractLogger implements \RedSerenity\Psr\Log\LoggerInterface
{

    /**
     * System is unusable.
     *
     * @param string $message
     * @param array $context
     * @return void
     */
    public function emergency($message, array $context = array()) {}

    /**
     * Action must be taken immediately.
     *
     * <p>Example: Entire website down, database unavailable, etc. This should
     * trigger the SMS alerts and wake you up.</p>
     *
     * @param string $message
     * @param array $context
     * @return void
     */
    public function alert($message, array $context = array()) {}

    /**
     * Critical conditions.
     *
     * <p>Example: Application component unavailable, unexpected exception.</p>
     *
     * @param string $message
     * @param array $context
     * @return void
     */
    public function critical($message, array $context = array()) {}

    /**
     * Runtime errors that do not require immediate action but should typically
     * be logged and monitored.
     *
     * @param string $message
     * @param array $context
     * @return void
     */
    public function error($message, array $context = array()) {}

    /**
     * Exceptional occurrences that are not errors.
     *
     * <p>Example: Use of deprecated APIs, poor use of an API, undesirable things
     * that are not necessarily wrong.</p>
     *
     * @param string $message
     * @param array $context
     * @return void
     */
    public function warning($message, array $context = array()) {}

    /**
     * Normal but significant events.
     *
     * @param string $message
     * @param array $context
     * @return void
     */
    public function notice($message, array $context = array()) {}

    /**
     * Interesting events.
     *
     * <p>Example: User logs in, SQL logs.</p>
     *
     * @param string $message
     * @param array $context
     * @return void
     */
    public function info($message, array $context = array()) {}

    /**
     * Detailed debug information.
     *
     * @param string $message
     * @param array $context
     * @return void
     */
    public function debug($message, array $context = array()) {}

}
