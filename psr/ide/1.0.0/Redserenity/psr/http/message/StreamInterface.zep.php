<?php

namespace RedSerenity\Psr\Http\Message;

/**
 * Describes a data stream.
 *
 * Typically, an instance will wrap a PHP stream; this interface provides
 * a wrapper around the most common operations, including serialization of
 * the entire stream to a string.
 *
 * Interface StreamInterface
 */
interface StreamInterface
{

    /**
     * Reads all data from the stream into a string, from the beginning to end.
     *
     * <p>This method <b>MUST</b> attempt to seek to the beginning of the stream before
     * reading data and read the stream until the end is reached.</p>
     *
     * <p>Warning: This could attempt to load a large amount of data into memory.</p>
     *
     * <p>This method <b>MUST NOT</b> raise an exception in order to conform with PHP's
     * string casting operations.</p>
     *
     * @see http://php.net/manual/en/language.oop5.magic.php#object.tostring
     * @return string
     */
    public function __toString();

    /**
     * Closes the stream and any underlying resources.
     *
     * @return void
     */
    public function close();

    /**
     * Separates any underlying resources from the stream.
     *
     * <p>After the stream has been detached, the stream is in an unusable state.</p>
     *
     * @return resource|null
     */
    public function detach();

    /**
     * Get the size of the stream if known.
     *
     * @return int|null
     */
    public function getSize();

    /**
     * Returns the current position of the file read/write pointer
     *
     * @throws \RuntimeException on error.
     * @return int
     */
    public function tell();

    /**
     * Returns true if the stream is at the end of the stream.
     *
     * @return bool
     */
    public function eof();

    /**
     * Returns whether or not the stream is seekable.
     *
     * @return bool
     */
    public function isSeekable();

    /**
     * Seek to a position in the stream.
     *
     * @link http://www.php.net/manual/en/function.fseek.php
     * @param int whence Specifies how the cursor position will be calculated
     *     based on the seek offset. Valid values are identical to the built-in
     *     PHP $whence values for `fseek()`.  SEEK_SET: Set position equal to
     *     offset bytes SEEK_CUR: Set position to current location plus offset
     *     SEEK_END: Set position to end-of-stream plus offset.
     * @throws \RuntimeException on failure.
     * @param int $offset Stream offset
     * @param int $whence
     */
    public function seek($offset, $whence = 0);

    /**
     * Seek to the beginning of the stream.
     *
     * If the stream is not seekable, this method will raise an exception;
     * otherwise, it will perform a seek(0).
     *
     * @see seek()
     * @link http://www.php.net/manual/en/function.fseek.php
     * @throws \RuntimeException on failure.
     */
    public function rewind();

    /**
     * Returns whether or not the stream is writable.
     *
     * @return bool
     */
    public function isWritable();

    /**
     * Write data to the stream.
     *
     * @throws \RuntimeException on failure.
     * @param string $string
     * @param string $$string The string that is to be written.
     * @return int
     */
    public function write($string);

    /**
     * Returns whether or not the stream is readable.
     *
     * @return bool
     */
    public function isReadable();

    /**
     * Read data from the stream.
     *
     * @param int length Read up to $length bytes from the object and return
     *     them. Fewer than $length bytes may be returned if underlying stream
     *     call returns fewer bytes.
     * @return string Returns the data read from the stream, or an empty string
     *     if no bytes are available.
     * @throws \RuntimeException if an error occurs.
     * @param int $length
     * @return string
     */
    public function read($length);

    /**
     * Returns the remaining contents in a string
     *
     * @throws \RuntimeException if unable to read or an error occurs while
     *     reading.
     * @return string
     */
    public function getContents();

    /**
     * Get stream metadata as an associative array or retrieve a specific key.
     *
     * <p>The keys returned are identical to the keys returned from PHP's
     * stream_get_meta_data() function.</p>
     *
     * @link http://php.net/manual/en/function.stream-get-meta-data.php
     * @return array|mixed|null Returns an associative array if no key is
     *     provided. Returns a specific key value if a key is provided and the
     *     value is found, or null if the key is not found.
     * @param string $key Specific metadata to retrieve.
     * @return array|mixed|null
     */
    public function getMetadata($key = null);

}
